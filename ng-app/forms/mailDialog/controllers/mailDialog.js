
app.controller('modalMailCtrl', function ($rootScope,$scope,$http, $modal, $log){

    //сформировать капчу на сервере
    $http.get(BASE_URL + "index.php/pages/generateCaptcha").success(function (json) {
    $rootScope.imageCaptcha = json.imgcode;
     });
          
    
    $scope.open = function (){

	    var modalInstance = $modal.open({
	        templateUrl: BASE_URL + "index.php/pages/show/send",
	        controller: 'ModalInstanceCtrl'
	        /*resolve: {
	              imageCaptcha: function () {
	              return $scope.imageCaptcha;
	          }
	        }*/
	     });
    }

});

app.controller('ModalInstanceCtrl', function ($rootScope,$scope,$http, $modalInstance){

          
    $scope.ok = function (answerForm) {
          
        var form_data = {
            email : answerForm.email,
            comment: answerForm.comment,
            captcha: answerForm.captcha
        };


        $http.post(BASE_URL + "index.php/pages/contact", form_data).success(function (json) {
 
 
           if (json.error==1) 
                { 

                    $rootScope.imageCaptcha = json.imgcode;
                    
                    $scope.msg = [json.message];
                    alert("Что-то пошло не так");


                    //доступ к элементам через element.

                   // $('#imag').html(json.imgcode).show();
                   
                    //$('#user_message').html(json.message).show();
                } else 
                {
                   
                   alert('Сообщение отправлено! ');
                   //$('#user_message').html(json.message).show();
                   $modalInstance.close($scope.tempJson);            
                }        
        });
    };


    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');            
    };

});
