var app = angular.module('appfrontend',['ui.bootstrap','ngRoute']);

//app.config(['$routeProvider', function($routeProvider) {
app.config(['$routeProvider','$locationProvider',function($routeProvider, $locationProvider){ 
 
    $routeProvider.otherwise({redirectTo: '/projects'});
    

    $routeProvider.when('/price', {
        templateUrl: BASE_URL + 'ng-app/staticpages/price.html',
       // templateUrl: BASE_URL + 'index.php/server/index/projects',
        controller: ''
    });

    $routeProvider.when('/signIn', {
        templateUrl: BASE_URL + 'ng-app/staticpages/signIn.html',
       // templateUrl: BASE_URL + 'index.php/server/index/projects',
        controller: ''
    });


    $routeProvider.when('/news', {
        templateUrl: BASE_URL + 'ng-app/staticpages/news.html',
       // templateUrl: BASE_URL + 'index.php/server/index/projects',
        controller: ''
    });

    $routeProvider.when('/projects', {
        templateUrl: BASE_URL + 'ng-app/projects/view/projects.html',
       // templateUrl: BASE_URL + 'index.php/server/index/projects',
        controller: 'projectsCtrl'
    });

    //http://localhost/bestway/index.php/projectscontroller/show/2/json
    http://localhost/bestway/ng-app/index.html#/project/2
    $routeProvider.when('/project/:id', {
        templateUrl: BASE_URL + 'ng-app/projects/view/project.html',
        controller: 'showprojectCtrl'
    });


    //.when('/edit/:id', {template: 'views/edit.html', controller: EditCtrl})

    $routeProvider.when('/about', {
        //templateUrl: BASE_URL + '/ng-app/staticpages/about.html',
        templateUrl: "staticpages/about.html",
        controller: ''
    });

    $routeProvider.when('/map', {
            templateUrl: BASE_URL + '/ng-app/staticpages/map.html',
            controller: ''
        });

}]);



app.controller('appCtrl', function ($rootScope){
   $rootScope.base_url = BASE_URL;
   
    $rootScope.canvasReady = function (){

      var canvas = document.getElementById('my_canvas');

      if (canvas.getContext){
         
           var context = canvas.getContext('2d');
  
           context.fillStyle = "rgb(200,0,0)";
           context.fillRect(10,10,100,100);
           context.fillStyle = "rgb(0,200,0)";
           context.fillRect(20,20,100,100);
           context.fillStyle = "rgb(0,0,200)";
           context.fillRect(30,30,100,100);

      }
       else{
        alert('no context');
       }
    }
});


/*
//Директива подгрузки странички по конкретному проекту
app.directive('projectcontext', ["$compile", function ($compile) 
{
    var html = "<p>Страница о себе</p>";

    return {
        restrict: 'A',
        link: function (scope, iElement, iAttrs) 
        {


            console.log("this func");
            html = "<p>Текст</p>";

            iElement.html(html);
            $compile(iElement.contents())(scope);
        }
    };
}]);
*/

app.directive('panel', ["$compile", function ($compile) {
    return {
        restrict:'E',
        transclude:true,
        scope:{ title:'@title' },
        
        link: function (scope, iElement, iAttrs) 
        {
            var html = "{{title}}";
           // var html = angular.copy(scope.datasource);

           // console.log(title);

         //   html += "<b>жирный текс</b>";

            html = "<div ng-include data-src=''></div>";

          //  var srcc = "'staticpages/projects/2.html'";

            //html = "<div ng-include data-src="+srcc+"></div>";

            
            iElement.html(html);
            $compile(iElement.contents())(scope);
        }
/*
        template:'<div class="panel">' +
            '<h3>{{projecthtml}}</h3>' +
            '<div class="panel-content" ng-transclude></div>' +
            '</div>'
*/
            ,
        replace:true
    };
}]);


























