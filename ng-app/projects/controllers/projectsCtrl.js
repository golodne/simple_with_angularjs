
app.controller('projectsCtrl',function($scope,$http,ProjectsFactory){
    
    $scope.list_data = [];
    

    ProjectsFactory.getProjects().success(function(result){
        $scope.list_data = result;

    });

});


app.controller('showprojectCtrl',function($scope,$http,ProjectsFactory,$routeParams){
    
    $scope.list_data = [];

    ProjectsFactory.showProject($routeParams.id).success(function(result){
        $scope.project_data = result;
    });
});



