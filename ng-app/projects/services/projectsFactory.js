


//Сервис для работы с проектами

app.factory('ProjectsFactory', function ProjectsFactory($http) {
  var factory = {};

  factory.getProjects = function () {

  return $http.get(BASE_URL + "index.php/projectscontroller/index/json");
  };

  factory.showProject = function(id) {
  return $http.get(BASE_URL + 'index.php/projectscontroller/show/' + id + '/json');
  };

  factory.imagesProject = function(project_id){
  return $http.get(BASE_URL + 'index.php/server/listFiles/' + project_id);
  };


  return factory;
})