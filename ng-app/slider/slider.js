
app.directive('flexslider', function ($compile,$routeParams,ProjectsFactory) {
  
  return {
    link: function (scope, element, attrs) {
      
    var projectId = $routeParams.id;


    //получение списка изображений по page_id
    ProjectsFactory.imagesProject(projectId).success(function(result){
        

    if (result.length > 0) {

      var el = angular.element(document.getElementById('space-for-image'));

  
      for (var i = 0; i < result.length; i++) {
                  
        image_name = result[i]['image_name'];

        var filepath = scope.base_url+"assets/img/projects/details/"+projectId+"/"+image_name;

        // el.append("<p>"+filepath+"</p>")
        el.append("<li> <img src="+filepath +' alt="Inspection Viewer" width="280" height="300" /></li>');

        $compile(el)(scope);
        element.append(el);
      };
      
         //инициализация слайдера
          element.flexslider({
            animation: "slide"//,
           // itemWidth: 200
        });


      }
 
    }); //end success imagesProject
    
          

    }
  }
});

/*
$.noConflict();


jQuery( document ).ready(


function( $ ) { // you passed $ as a parameter for jQuery
alert('dfdfd');
  });
*/

 